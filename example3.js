import {
	enrichWithSomeParams,
	getLogger,
	getPartialForDataA,
	getPartialForDataB,
	getSomeData,
	mapToCombinedData
} from './functions';

const fallbackData = null;

/**
	* original, dumbed down code
	*/
function a(dataId) {
	return getSomeData(dataId)
		.then((data) => Promise.all([
			getPartialForDataA(data),
			getPartialForDataB(data)
		]))
		.then(([partialA, partialB]) => mapToCombinedData(partialA, partialB))
		.then(enrichWithSomeParams)
		.catch((err) => {
			getLogger().error({err});

			return Promise.resolve(fallbackData);
		});
}

/**
	* better code imo
	*/
function b(dataId) {
	return getSomeData(dataId)
		.then((data) => getPartialData(data))
		.then(([pA, pB]) => mapToCombinedData(pA, pB))
		.then((combinedData) => enrichWithSomeParams(combinedData))
		.catch((err) => handleError(err));

	function getPartialData(data) {
		return Promise.all([
			getPartialForDataA(data),
			getPartialForDataB(data)
		]);
	}

	function handleError() {
		getLogger().error({err});

		return Promise.resolve(fallbackData);
	}
}

/**
	* eslint valid code
	*/
function c(dataId) {
	return getSomeData(dataId)
		.then((data) => {
			return getPartialData(data);
		})
		.then(([partialA, partialB]) => {
			return mapToCombinedData(partialA, partialB);
		})
		.then((combinedData) => {
			return enrichWithSomeParams(combinedData);
		})
		.catch((err) => {
			getLogger().error({
				'err': err
			});

			return Promise.resolve(fallbackData);
		});

	function getPartialData(data) {
		return Promise.all([
			getPartialForDataA(data),
			getPartialForDataB(data)
		]);
	}
}

a();
b();
c();
