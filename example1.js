import {
	DossierAction,
	deleteFromExternalServices,
	dossierRepository,
	getLogger,
	notifyNavigationTopic
} from './functions';

/**
	* original code
	*/
export function removeA(id) {
	getLogger().debug({id}, `Removing dossier '${id}'`);

	return dossierRepository
		.findDossierById(id)
		.then(deleteFromExternalServices)
		.then(() => notifyNavigationTopic({id, action: DossierAction.DELETE}));
}

/**
	* eslint valid code
	*/
export function removeB(id) {
	getLogger().debug({
		'id': id
	}, `Removing dossier '${id}'`);

	return dossierRepository
		.findDossierById(id)
		.then((dossier) => {
			return deleteFromExternalServices(dossier);
		})
		.then(() => {
			notifyNavigationTopic({
				'id': id,
				'action': DossierAction.DELETE
			});
		});
}
