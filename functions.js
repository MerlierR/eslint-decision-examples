export function getLogger() {
	return {};
}

export function notifyNavigationTopic() {
	return null;
}

export function deleteFromExternalServices() {
	return null;
}

export function _onCacheExpired() {
	return null;
}

export function getSomeData() {
	return null;
}

export function getPartialForDataA() {
	return null;
}

export function getPartialForDataB() {
	return null;
}

export function enrichWithSomeParams() {
	return null;
}

export function mapToCombinedData() {
	return null;
}

export const dossierRepository = {};
export const DossierAction = {};