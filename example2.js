import {getLogger} from './functions';

class Cache {
	/**
		* Original code, valid, with arrow functions
		*/
	addAndReturnLatestValue(key, getValue) {
		return getValue()
			.then((value) => {
				this.addToCache(key, value, this._onCacheExpired);

				return value;
			})
			.catch((err) => {
				getLogger().warn(`Could not add value for key "${key}" to ${this.name}`);
				getLogger().error({err});

				return Promise.reject(err);
			});
	}

	/**
		* better code, invalid, but more readable imo: the core is returned almost directly
		*/
	addAndReturnLatestValue2(key, getValue) {
		console.log(this, 'is', 'used'); // class-methods-use-this

		return getValue()
			.then((value) => addValuetoCache(value)) // arrow function to force 'this'
			.catch((err) => handleError(err));

		function addValuetoCache(value) {
			this.addToCache(key, value, this._onCacheExpired);

			return value;
		}

		function handleError(err) {
			getLogger().warn(`Could not add value for key "${key}" to ${this.name}`);
			getLogger().error({err});

			return Promise.reject(err);
		}
	}

	/**
		* eslint valid code for 2nd version
		*/
	addAndReturnLatestValue3(key, getValue) {
		console.log(this, 'is', 'used'); // class-methods-use-this

		return getValue()
			.then((value) => {
				return addValuetoCache(value);
			})
			.catch((err) => {
				return handleError(err);
			});

		function addValuetoCache(value) {
			this.addToCache(key, value, this._onCacheExpired);

			return value;
		}

		function handleError(err) {
			getLogger().warn(`Could not add value for key "${key}" to ${this.name}`);
			getLogger().error({err});

			return Promise.reject(err);
		}
	}

	/**
		* eslint valid code, playing around with 'bind'. Backenders don't really like bind, apply and call
		*/
	addAndReturnLatestValue4(key, getValue) {
		return getValue()
			.then(addValuetoCache.bind(this)) // function param ==> 'this' could be anything ==> bind
			.catch(handleError.bind(this));

		function addValuetoCache(value) {
			this.addToCache(key, value, this._onCacheExpired);

			return value;
		}

		function handleError(err) {
			getLogger().warn(`Could not add value for key "${key}" to ${this.name}`);
			getLogger().error({
				'err': err
			});

			return Promise.reject(err);
		}
	}

	/**
		* @private
		*/
	_onCacheExpired(expiredKey, expiredValue) {
		console.log(this, expiredKey, expiredValue);

		return null;
	}
}

new Cache();
